/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printUserInfo(){
		let firstName = prompt('What is your name?');
		let age = prompt('How old are you?');
		let address = prompt('Where do you live?');
		alert('Thank you for your input!');
		console.log("Hello, " + firstName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	}

	printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//second function here:

	function topFiveFavoriteBands(){
		console.log("1. The Beatles");
		console.log("2. Metallica");
		console.log("3. The Eagles");
		console.log("4. L'arc~en~Ciel");
		console.log("5. Eraserheads")
	} 

	topFiveFavoriteBands();

	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function topFiveFavoriteMovies(){
		console.log("1. The Godfather");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("2. The Godfather, Part II");
		console.log("Rotten Tomatoes Rating: 96%");
		console.log("3. Shawshank Redemption");
		console.log("Rotten Tomatoes Rating: 91%");
		console.log("4. To Kill a Mockingbird");
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("5. Psycho");
		console.log("Rotten Tomatoes Rating: 96%");
	}

	topFiveFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

/*
This code is unusable since it is outside the function scope
console.log(friend1);
console.log(friend2);
*/